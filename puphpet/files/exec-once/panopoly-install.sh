#!/bin/bash
#
# Installs Drupal 7 using drush.

#
# Variables below correspond to values in PuPHPet's config.yml.
#

# User who has write access to install Drupal.
USER=vagrant

# Group of your Apache Server.
GROUP=www-data

# Drupal install directory.
DRUPAL_DIR=/var/www/web

# Drupal URL.
DRUPAL_URI=local.dev

#Drupal site name.
DRUPAL_NAME="Drupal 7"

# MySQL DB name and credentials.
DRUPAL_MYSQL_USER=dbuser
DRUPAL_MYSQL_PASSWORD=123
DRUPAL_MYSQL_DB=dbname

# Drupal administrator's credentials.
DRUPAL_USER_NAME=admin
DRUPAL_USER_PASSWORD=admin
DRUPAL_USER_MAIL=admin@example.com

# Drupal release to use: drupal, drupal-7.x etc.
DRUPAL_RELEASE=drupal

# Start installation.
echo '****** PANOPOLY INSTALLATION ******'

# Modify user to make sure that there will be no permission issues.
sudo usermod -a -G ${GROUP} ${USER}

# Remove any contents from docroot, if exist.
sudo rm -Rf ${DRUPAL_DIR}/*

# Create Panopoly docroot and assign permissions.
sudo sh -c "mkdir -p ${DRUPAL_DIR}; chmod -f 775 ${DRUPAL_DIR}; chown -f ${USER}:${GROUP} ${DRUPAL_DIR}; chmod g+s ${DRUPAL_DIR}"

# Download Panopoly.
drush dl panopoly --destination="`dirname ${DRUPAL_DIR}`" --drupal-project-rename="`basename ${DRUPAL_DIR}`" -y

# Download Panopoly Demo.
drush dl panopoly_demo --destination="${DRUPAL_DIR}/sites/all/modules" -y

# Install Panopoly.
drush site-install panopoly --root=${DRUPAL_DIR} --uri=http://${DRUPAL_URI} --db-url=mysql://${DRUPAL_MYSQL_USER}:${DRUPAL_MYSQL_PASSWORD}@localhost/${DRUPAL_MYSQL_DB} --account-name=${DRUPAL_USER_NAME} --account-pass=${DRUPAL_USER_PASSWORD} --account-mail=${DRUPAL_USER_MAIL} --site-mail=${DRUPAL_USER_MAIL} --site-name=${DRUPAL_NAME} -y

# Change permissions for files directory.
sudo sh -c "chown -R ${WEB_USER}:${WEB_GROUP} ${DRUPAL_DIR}/sites/default/files; chmod g+s ${DRUPAL_DIR}/sites/default/files"

# Download and install theme
rm -rf "${DRUPAL_DIR}/sites/all/themes/base"
git clone https://bitbucket.org/jblpdev/drupal-v7-panopoly-theme.git "${DRUPAL_DIR}/sites/all/themes/base"
rm -rf "${DRUPAL_DIR}/sites/all/themes/base/.git"

# Show information about installed Drupal.
drush status --root=${DRUPAL_DIR}